from django.shortcuts import render


def home_page_view(request):
    return render(request, 'pages/home.html')

def about_page_view(request):
    context = {
        'page_name' : 'About',
        'description': 'This is something said in context',
        'button_value' : "Don't click me!"
    }

    return render(request, 'pages/about.html', context) 

def contactus_page_view(request):
    return render(request, 'pages/contactus.html')







# Create your views here.
